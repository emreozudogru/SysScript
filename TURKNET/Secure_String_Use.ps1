﻿$AdminUser = "admin@tenant.onmicrosoft.com"

try 
	{	 
		$PasswordFile = ($PSScriptRoot + "\$adminuser.txt")
		if (!(Test-Path  -Path $passwordfile))
			{
				Write-Host "You don't have an admin password assigned for $adminuser, please provide the password followed with enter below:" -Foregroundcolor Yellow
				Read-Host -assecurestring | convertfrom-securestring | out-file $passwordfile
			}
		$password = get-content $passwordfile | convertto-securestring -ErrorAction Stop
		$credentials = new-object -typename System.Management.Automation.PSCredential -argumentlist $adminuser,$password -ErrorAction Stop
		Write-Host $credentials $adminuser $password
	} 
	catch [system.exception]
	{
        $err = $_.Exception.Message
		Write-Host "Error: Could not Connect to Office 365, please check connection, username and password.`r`nError: $err"
        exit
	}
